﻿namespace ILikeToMoveIt1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.plGameStart = new System.Windows.Forms.Panel();
            this.pbGame = new System.Windows.Forms.PictureBox();
            this.btnStartGame = new System.Windows.Forms.Button();
            this.trGameStart = new System.Windows.Forms.Timer(this.components);
            this.trBarGameStart = new System.Windows.Forms.TrackBar();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.btnBackColorShape = new System.Windows.Forms.Button();
            this.grBoxShape = new System.Windows.Forms.GroupBox();
            this.rBtnRectangle = new System.Windows.Forms.RadioButton();
            this.rBtnCircle = new System.Windows.Forms.RadioButton();
            this.rBtnSquare = new System.Windows.Forms.RadioButton();
            this.btnBorderColorShape = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnPanelColor = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gameInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.authorInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.plGameStart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbGame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trBarGameStart)).BeginInit();
            this.grBoxShape.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // plGameStart
            // 
            this.plGameStart.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.plGameStart.Controls.Add(this.pbGame);
            this.plGameStart.Location = new System.Drawing.Point(12, 53);
            this.plGameStart.Name = "plGameStart";
            this.plGameStart.Size = new System.Drawing.Size(675, 417);
            this.plGameStart.TabIndex = 0;
            this.plGameStart.Paint += new System.Windows.Forms.PaintEventHandler(this.plGameStart_Paint);
            // 
            // pbGame
            // 
            this.pbGame.BackColor = System.Drawing.Color.Transparent;
            this.pbGame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbGame.Location = new System.Drawing.Point(70, 82);
            this.pbGame.Name = "pbGame";
            this.pbGame.Size = new System.Drawing.Size(32, 32);
            this.pbGame.TabIndex = 0;
            this.pbGame.TabStop = false;
            // 
            // btnStartGame
            // 
            this.btnStartGame.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnStartGame.Location = new System.Drawing.Point(693, 53);
            this.btnStartGame.Name = "btnStartGame";
            this.btnStartGame.Size = new System.Drawing.Size(118, 39);
            this.btnStartGame.TabIndex = 1;
            this.btnStartGame.Text = "Start Game";
            this.btnStartGame.UseVisualStyleBackColor = true;
            this.btnStartGame.Click += new System.EventHandler(this.btnStartGame_Click);
            // 
            // trGameStart
            // 
            this.trGameStart.Interval = 1;
            this.trGameStart.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // trBarGameStart
            // 
            this.trBarGameStart.Location = new System.Drawing.Point(722, 114);
            this.trBarGameStart.Maximum = 2;
            this.trBarGameStart.Name = "trBarGameStart";
            this.trBarGameStart.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trBarGameStart.Size = new System.Drawing.Size(56, 104);
            this.trBarGameStart.TabIndex = 2;
            this.trBarGameStart.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trBarGameStart.Scroll += new System.EventHandler(this.trBarGameStart_Scroll);
            // 
            // btnBackColorShape
            // 
            this.btnBackColorShape.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBackColorShape.Location = new System.Drawing.Point(693, 233);
            this.btnBackColorShape.Name = "btnBackColorShape";
            this.btnBackColorShape.Size = new System.Drawing.Size(118, 23);
            this.btnBackColorShape.TabIndex = 3;
            this.btnBackColorShape.Text = "Back Color";
            this.btnBackColorShape.UseVisualStyleBackColor = true;
            this.btnBackColorShape.Click += new System.EventHandler(this.button1_Click);
            // 
            // grBoxShape
            // 
            this.grBoxShape.Controls.Add(this.rBtnRectangle);
            this.grBoxShape.Controls.Add(this.rBtnCircle);
            this.grBoxShape.Controls.Add(this.rBtnSquare);
            this.grBoxShape.Location = new System.Drawing.Point(696, 352);
            this.grBoxShape.Name = "grBoxShape";
            this.grBoxShape.Size = new System.Drawing.Size(115, 107);
            this.grBoxShape.TabIndex = 4;
            this.grBoxShape.TabStop = false;
            this.grBoxShape.Text = "Shape";
            // 
            // rBtnRectangle
            // 
            this.rBtnRectangle.AutoSize = true;
            this.rBtnRectangle.Location = new System.Drawing.Point(8, 77);
            this.rBtnRectangle.Name = "rBtnRectangle";
            this.rBtnRectangle.Size = new System.Drawing.Size(93, 21);
            this.rBtnRectangle.TabIndex = 2;
            this.rBtnRectangle.TabStop = true;
            this.rBtnRectangle.Text = "Rectangle";
            this.rBtnRectangle.UseVisualStyleBackColor = true;
            // 
            // rBtnCircle
            // 
            this.rBtnCircle.AutoSize = true;
            this.rBtnCircle.Location = new System.Drawing.Point(7, 50);
            this.rBtnCircle.Name = "rBtnCircle";
            this.rBtnCircle.Size = new System.Drawing.Size(64, 21);
            this.rBtnCircle.TabIndex = 1;
            this.rBtnCircle.TabStop = true;
            this.rBtnCircle.Text = "Circle";
            this.rBtnCircle.UseVisualStyleBackColor = true;
            // 
            // rBtnSquare
            // 
            this.rBtnSquare.AutoSize = true;
            this.rBtnSquare.Location = new System.Drawing.Point(7, 22);
            this.rBtnSquare.Name = "rBtnSquare";
            this.rBtnSquare.Size = new System.Drawing.Size(75, 21);
            this.rBtnSquare.TabIndex = 0;
            this.rBtnSquare.TabStop = true;
            this.rBtnSquare.Text = "Square";
            this.rBtnSquare.UseVisualStyleBackColor = true;
            // 
            // btnBorderColorShape
            // 
            this.btnBorderColorShape.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBorderColorShape.Location = new System.Drawing.Point(693, 273);
            this.btnBorderColorShape.Name = "btnBorderColorShape";
            this.btnBorderColorShape.Size = new System.Drawing.Size(118, 23);
            this.btnBorderColorShape.TabIndex = 5;
            this.btnBorderColorShape.Text = "Border Color";
            this.btnBorderColorShape.UseVisualStyleBackColor = true;
            this.btnBorderColorShape.Click += new System.EventHandler(this.btnBorderColorShape_Click);
            // 
            // btnPanelColor
            // 
            this.btnPanelColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPanelColor.Location = new System.Drawing.Point(693, 313);
            this.btnPanelColor.Name = "btnPanelColor";
            this.btnPanelColor.Size = new System.Drawing.Size(118, 23);
            this.btnPanelColor.TabIndex = 6;
            this.btnPanelColor.Text = "Color";
            this.btnPanelColor.UseVisualStyleBackColor = true;
            this.btnPanelColor.Click += new System.EventHandler(this.btnPanelColor_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(820, 28);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameInfoToolStripMenuItem,
            this.authorInfoToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // gameInfoToolStripMenuItem
            // 
            this.gameInfoToolStripMenuItem.Name = "gameInfoToolStripMenuItem";
            this.gameInfoToolStripMenuItem.Size = new System.Drawing.Size(159, 26);
            this.gameInfoToolStripMenuItem.Text = "Game Info";
            this.gameInfoToolStripMenuItem.Click += new System.EventHandler(this.gameInfoToolStripMenuItem_Click);
            // 
            // authorInfoToolStripMenuItem
            // 
            this.authorInfoToolStripMenuItem.Name = "authorInfoToolStripMenuItem";
            this.authorInfoToolStripMenuItem.Size = new System.Drawing.Size(159, 26);
            this.authorInfoToolStripMenuItem.Text = "Author Info";
            this.authorInfoToolStripMenuItem.Click += new System.EventHandler(this.authorInfoToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(820, 482);
            this.Controls.Add(this.btnPanelColor);
            this.Controls.Add(this.btnBorderColorShape);
            this.Controls.Add(this.grBoxShape);
            this.Controls.Add(this.btnBackColorShape);
            this.Controls.Add(this.trBarGameStart);
            this.Controls.Add(this.btnStartGame);
            this.Controls.Add(this.plGameStart);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ILikeToMoveIt";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing_1);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.plGameStart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbGame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trBarGameStart)).EndInit();
            this.grBoxShape.ResumeLayout(false);
            this.grBoxShape.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel plGameStart;
        private System.Windows.Forms.Button btnStartGame;
        private System.Windows.Forms.PictureBox pbGame;
        private System.Windows.Forms.Timer trGameStart;
        private System.Windows.Forms.TrackBar trBarGameStart;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button btnBackColorShape;
        private System.Windows.Forms.GroupBox grBoxShape;
        private System.Windows.Forms.RadioButton rBtnRectangle;
        private System.Windows.Forms.RadioButton rBtnCircle;
        private System.Windows.Forms.RadioButton rBtnSquare;
        private System.Windows.Forms.Button btnBorderColorShape;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnPanelColor;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gameInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem authorInfoToolStripMenuItem;
    }
}

