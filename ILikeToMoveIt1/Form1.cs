﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ILikeToMoveIt1
{
    public partial class Form1 : Form
    {
        private int x = 1;
        private int y = 1;
        Pen pen = new Pen(Color.Green, 4);
        SolidBrush br = new SolidBrush(Color.Red);
        public static Form2 f2 = new Form2();
        //private Color clr = Color.Red;

        public Form1()
        {
            InitializeComponent();
            //pbGame.BackColor = Color.Red;
        }

        private void btnStartGame_Click(object sender, EventArgs e)
        {
            if (btnStartGame.Text == "Stop Game")
            {
                trGameStart.Enabled = false;
                btnStartGame.Text = "Start Game";
            }
            else if (btnStartGame.Text == "Start Game")
            {
                trGameStart.Enabled = true;
                btnStartGame.Text = "Stop Game";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pbGame.Left += x;
            pbGame.Top += y;
            if (rBtnCircle.Checked || rBtnSquare.Checked)
            {
                if (pbGame.Location.X >= 480)
                {
                    x = -x;
                }
                else if (pbGame.Location.Y >= 310)
                {
                    y = -y;
                }
                else if (pbGame.Location.X <= 0)
                {
                    x = Math.Abs(x);
                }
                else if (pbGame.Location.Y <= 0)
                {
                    y = Math.Abs(y);
                }
            }
            else
            {
                if (pbGame.Location.X >= 470)
                {
                    x = -x;
                }
                else if (pbGame.Location.Y >= 300)
                {
                    y = -y;
                }
                else if (pbGame.Location.X <= 0)
                {
                    x = Math.Abs(x);
                }
                else if (pbGame.Location.Y <= 0)
                {
                    y = Math.Abs(y);
                }
            }
            plGameStart.Invalidate();
        }

        private void trBarGameStart_Scroll(object sender, EventArgs e)
        {
            int tbGameStart = trBarGameStart.Value;
            switch (tbGameStart)
            {
                case 0:
                    x = 1;  y = 1;
                    break;
                case 1:
                    x = 2; y = 2;
                    break;
                case 2:
                    x = 3; y = 3;
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            br.Color = colorDialog1.Color;
        }

        private void plGameStart_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Rectangle rect = new Rectangle(pbGame.Location.X, pbGame.Location.Y, pbGame.Width, pbGame.Height);
            Rectangle rect1 = new Rectangle(pbGame.Location.X, pbGame.Location.Y, pbGame.Width + 10, pbGame.Height);

            if (rBtnSquare.Checked)
            {
                g.DrawRectangle(pen, rect);
                g.FillRectangle(br, rect);
            }
            else if (rBtnCircle.Checked)
            {
                g.DrawEllipse(pen, rect);
                g.FillEllipse(br, rect);
            }
            else if (rBtnRectangle.Checked)
            {
                g.DrawRectangle(pen, rect1);
                g.FillRectangle(br, rect1);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            rBtnSquare.Checked = true;
        }

        private void btnBorderColorShape_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            pen.Color = colorDialog1.Color;
        }

        private void btnPanelColor_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            plGameStart.BackColor = colorDialog1.Color;
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f2.ShowDialog();
        }

        private void gameInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 f3 = new Form3();
            f3.Show();
        }

        private void authorInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form4 f4 = new Form4();
            f4.Show();
        }
        private void Form1_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            DialogResult dr = MessageBox.Show("Do you really want to exit?", "Exit Game", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            Form1 f1 = new Form1();
            if (dr == DialogResult.Yes)
            {
                f1.Close();
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
